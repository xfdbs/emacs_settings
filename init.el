﻿;;C:\Users\Administrator\AppData\Roaming\.emacs
;;(load-file "D:/Program/emacs-24.5/.emacs")
;;上面一句，用来指定当前.emacs位置
;;也可以直接把下面配置到第一行的文件里面

;; Set defalut font
;(set-default-font "字体名称-字号")
;(set-default-font "Consolas-11")

;;显示行号
(global-linum-mode t)  

;;禁用自动备份
(setq make-backup-files nil)

;;关闭welcome界面
(setq inhibit-startup-message t)

;;evil vim模式
(add-to-list 'load-path "~/.emacs.d/plugin/evil") 
(require 'evil) 
(evil-mode 1) 

;;缩进
(setq indent-tabs-mode nil)
(setq c-basic-offset 4)
(setq tab-width 4)

;;主题
(custom-set-variables
'(custom-enabled-themes (quote (tango-dark))))

;; 设置垃圾回收，在Windows下，emacs25版本会频繁出发垃圾回收，所以需要设置
(when (eq system-type 'windows-nt)
(setq gc-cons-threshold (* 512 1024 1024))
(setq gc-cons-percentage 0.5)
(run-with-idle-timer 5 t #'garbage-collect)
;; 显示垃圾回收信息，这个可以作为调试用
;; (setq garbage-collection-messages t)
)

;;切换窗口 window-numbering
(add-to-list 'load-path "~/.emacs.d/plugin/window-numbering")
(require 'window-numbering)
(window-numbering-mode t)  

;;neotree 目录树
(add-to-list 'load-path "~/.emacs.d/plugin/neotree")
(require 'neotree)
(global-set-key [f3] 'neotree-toggle)
(add-hook 'neotree-mode-hook
  (lambda ()
    (define-key evil-normal-state-local-map (kbd "TAB") 'neotree-enter)
    (define-key evil-normal-state-local-map (kbd "SPC") 'neotree-enter)
    (define-key evil-normal-state-local-map (kbd "q") 'neotree-hide)
    (define-key evil-normal-state-local-map (kbd "RET") 'neotree-enter)))



;;popup, auto-complete需要
(load-file "~/.emacs.d/plugin/popup/popup.el")

;; auto-complete 
(add-to-list 'load-path "~/.emacs.d/plugin/auto-complete")
;;(require 'auto-complete)
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/plugin/auto-complete/ac-dict")
(ac-config-default)

(add-to-list 'load-path
	     "~/.emacs.d/plugin/yasnippet")
(require 'yasnippet)
(yas-global-mode 1) 

;;auto-complete-clang-async
(add-to-list 'load-path "~/.emacs.d/plugin/auto-complete-clang-async")
(require 'auto-complete-clang-async)
(defun ac-cc-mode-setup ()
  (setq ac-clang-complete-executable "~/.emacs.d/plugin/auto-complete-clang-async/clang-complete")
  (setq ac-sources '(ac-source-clang-async))
  (ac-clang-launch-completion-process)
  )
(defun my-ac-config ()
  (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t))
(my-ac-config)  

;;markdown-mode
(add-to-list 'load-path "~/.emacs.d/plugin/markdown-mode")
(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)
;(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
;;install pandoc first
(custom-set-variables
  '(markdown-command "/usr/bin/pandoc"))
