# !/usr/bin/sh

#install packages
if which apt > /dev/null
then
	sudo apt install pandoc emacs clang libclang-dev llvm-dev unzip
elif which dnf > /dev/null
then
	sudo dnf install pandoc emacs clang clang-devel llvm-devel unzip redhat-rpm-config
elif which yum > /dev/null
then
	sudo yum install pandoc emacs clang clang-devel llvm-devel unzip redhat-rpm-config
fi

rm -rf ~/.emacs.d_old
mv ~/.emacs.d ~/.emacs.d_old
mkdir ~/.emacs.d
cp -r init.el plugin ~/.emacs.d
unzip source/auto-complete-clang-async.zip  -d source/
cd source/emacs-clang-complete-async-master/
make
cp clang-complete ~/.emacs.d/plugin/auto-complete-clang-async/
cd ../..
rm -rf source/emacs-clang-complete-async-master/
rm -rf plugin/auto-complete-clang-async/clang-complete
